import express from 'express';
import {main} from './NodeMailer/NodeMailer.js';
const port=process.env.PORT || 3000;
const app=express();
import {User} from './config/firebase1.js';

app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.set('view engine','ejs');
app.set('views','./views');

app.get('/',(req,res)=>{
    res.render('index');
});

app.post('/form',async(req,res)=>{
      let data=await User.add(req.body);
      console.log(req.body.email)
      main(req.body.email).catch(console.error);
      res.status(200).send("Data has added to database");

});
app.listen(port,()=>{
    console.log(`App is running at port ${port}`);
})